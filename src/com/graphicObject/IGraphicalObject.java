package com.graphicObject;

import java.awt.Color;
import java.awt.Graphics;

public abstract class IGraphicalObject {

	private Color color;
	private int phID;

	public IGraphicalObject(int phID) {
		this.phID = phID;
		setColor(phID);
	}

	public void draw(Graphics g) {
	}

	public int getPhID() {
		return phID;
	}

	public void setPhID(int phID) {
		this.phID = phID;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(int phID) {
		this.phID = phID;
		if (phID == -1) {
			this.color = Color.black;
		} else if (phID == 0) {
			this.color = Color.red;
		} else if (phID == 1) {
			this.color = Color.blue;
		} else if (phID == 2) {
			this.color = Color.green;
		} else if (phID == 3) {
			this.color = Color.yellow;
		} else if (phID == 4) {
			this.color = Color.white;
		}
	}
}
