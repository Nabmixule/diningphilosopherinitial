package com.GUI;

import java.awt.*;

import com.graphicObject.IGraphicalObject;

class GraphicPlate extends IGraphicalObject {
	
	private final static int PLATE_POS_X = 10;
	private final static int PLATE_POS_Y = 5;
	private final static int PLATE_POS = 72;
	
	private Point coords;
	private int angle;
	private int size;

	public GraphicPlate(int ident, Point center, Point coords, int size) {
		super(-1);
		this.size = size;
		super.setPhID(-1);
		this.angle = PLATE_POS * ident;
		this.coords = new Point(Graphic2D.rotate(coords.x, coords.y, center.x, center.y, angle));
		this.coords.x -= PLATE_POS_X;
		this.coords.y += PLATE_POS_Y;
	}

	public void draw(Graphics g) {
		g.setColor(super.getColor());
		g.fillOval(coords.x, coords.y, size, size);
	}

}
