package com.GUI;

import java.awt.*;
import java.awt.event.*;

import com.event.IEvent;
import com.graphicObject.IGraphicalObject;

public class GraphicTable extends Frame implements IEvent {

	private static final long serialVersionUID = 4321094324106193221L;
	private static final int DIMENSION = 200;
	private static final int ADJUST = 70;
	private static final int CHOP_ADJUST = 40;
	private static final int PLATE_ADJUST = 20;
	private static final int DIVIDER = 2;
	
	private Point center;
	private IGraphicalObject plates[];
	private IGraphicalObject chops[];

	@SuppressWarnings({ "deprecation" })
	public GraphicTable() {
		super();

		addWindowListener(this);
		setTitle("Dining Philosophers");
		setSize(DIMENSION, DIMENSION);
		setBackground(Color.darkGray);

		center = new Point(getSize().width / DIVIDER, getSize().height / DIVIDER);

		fillPlates();
		fillChops();

		show();
		setResizable(false);
	}

	private void fillChops() {
		chops = new GraphicChopstick[5];
		for (int i = 0; i < 5; i++) {
			chops[i] = new GraphicChopstick(i, center, new Point(center.x, center.y - ADJUST),
					new Point(center.x, center.y - CHOP_ADJUST));
		}
	}

	private void fillPlates() {
		plates = new GraphicPlate[5];
		for (int i = 0; i < 5; i++) {
			plates[i] = new GraphicPlate(i, center, new Point(center.x, center.y - ADJUST), PLATE_ADJUST);
		}
	}

	public void windowClosing(WindowEvent evt) {
		System.exit(0);
	}

	public void isHungry(int phID) {
		plates[phID].setColor(phID);
		repaint();
	}

	public void isThinking(int phID) {
		plates[phID].setColor(-1);
		repaint();
	}

	public void takeChopstick(int phID, int chID) {
		chops[chID].setColor(phID);
		repaint();
	}

	public void releaseChopstick(int phID, int chID) {
		chops[chID].setColor(-1);
		repaint();
	}

	public void paint(Graphics g) {
		for (int i = 0; i < 5; i++) {
			plates[i].draw(g);
			chops[i].draw(g);
		}
	}

}
