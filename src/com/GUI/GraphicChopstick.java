package com.GUI;

import java.awt.*;

import com.graphicObject.IGraphicalObject;

class GraphicChopstick extends IGraphicalObject{
	
	private final static int CHOP_ADJUST_X = 72;
	private final static int CHOP_ADJUST_Y = 36;
	private final static int CHOP_POS_Y = 15;
	
	
	private Point coordStart;
	private Point coordEnd;
	private int angle;

	GraphicChopstick(int id, Point center, Point coordStart, Point coordEnd) {
		super(-1);

		this.angle = CHOP_ADJUST_X * id + CHOP_ADJUST_Y;
		this.coordStart = new Point(Graphic2D.rotate(coordStart.x, coordStart.y, center.x, center.y, angle));
		this.coordStart.y += CHOP_POS_Y;

		this.coordEnd = new Point(Graphic2D.rotate(coordEnd.x, coordEnd.y, center.x, center.y, angle));
		this.coordEnd.y += CHOP_POS_Y;
	}

	public void draw(Graphics g) {
		g.setColor(super.getColor());
		g.drawLine(coordStart.x, coordStart.y, coordEnd.x, coordEnd.y);
	}

}
