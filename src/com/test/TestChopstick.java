package com.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.bean.Chopstick;

public class TestChopstick {

	private Chopstick chopstick;

	@Before
	public void setUp() throws Exception {
		chopstick = new Chopstick(0);
	}

	@After
	public void tearDown() throws Exception {
		chopstick = null;
	}

	@Test
	public void testTake() throws Exception {
		chopstick.take();
		assertFalse(chopstick.isLibre());
	}

	@Test
	public void testRelease() throws Exception {
		chopstick.take();
		chopstick.release();
		assertTrue(chopstick.isLibre());
	}

}
