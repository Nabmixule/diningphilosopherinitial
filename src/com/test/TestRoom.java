package com.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.bean.Room;

public class TestRoom {
	
	private Room room;
	
	@Before
	public void setUp() throws Exception {
		room = new Room();
	}

	@After
	public void tearDown() throws Exception {
		room = null;
	}

	@Test
	public void testEnter() {
		room.entrer();
		assertFalse(room.getCompte() == 0);
	}

	@Test
	public void testExit() {
		room.entrer();
		room.sortir();
		assertTrue(room.getCompte() == 0);
	}

}
