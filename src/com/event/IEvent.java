package com.event;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public interface IEvent extends WindowListener {
	public void windowClosing(WindowEvent evt);

	default void windowOpened(WindowEvent e) {
	}

	default void windowClosed(WindowEvent e) {
	}

	default void windowIconified(WindowEvent e) {
	}

	default void windowDeiconified(WindowEvent e) {
	}

	default void windowActivated(WindowEvent e) {
	}

	default void windowDeactivated(WindowEvent e) {
	}

}
