package com.bean;

public class Chopstick {
	
	private int id;
	private boolean libre;

	public Chopstick(int id) {
		this.id = id;
		this.libre = true;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	public synchronized void take() {
		while (!this.libre) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("boom !");
			}
		}
		this.libre = false;

	}

	public synchronized void release() {
		notifyAll();
		this.libre = true;

	}

	public int getID() {
		return (this.id);
	}
}
