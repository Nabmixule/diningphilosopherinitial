package com.bean;

public class Room {

	private final int MAX = 4;

	private int compte;
	private int enAttente;

	public Room() {
		this.compte = 0;
		this.enAttente = 0;
	}

	public synchronized void entrer() {
		while (this.compte == MAX || this.enAttente > 0) {
			try {
				this.enAttente++;
				wait();
				this.enAttente--;
			} catch (InterruptedException e) {
				System.out.println(e);
			}
		}
		this.compte++;
	}

	public synchronized void sortir() {
		this.compte--;
		notify();
	}

	public int getCompte() {
		return compte;
	}

	public void setCompte(int compte) {
		this.compte = compte;
	}

	public int getEnAttente() {
		return enAttente;
	}

	public void setEnAttente(int enAttente) {
		this.enAttente = enAttente;
	}
}
