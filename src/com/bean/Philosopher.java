package com.bean;

import com.GUI.GraphicTable;

public class Philosopher extends Thread {

	private final int TIME_THINK_MAX = 5000;
	private final int TIME_NEXT_FORK = 100;
	private final int TIME_EAT_MAX = 2000;

	private int id;
	private GraphicTable table;
	private Chopstick left, right;
	private Room room;

	public Philosopher(int id, GraphicTable table, Chopstick left, Chopstick right, Room room) {
		this.id = id;
		this.table = table;
		this.left = left;
		this.right = right;
		this.room = room;
		setName("Philosopher " + id);
	}

	public void run() {
		for (;;) {
			isThinking();
			room.entrer();
			wantsFood();
			getChops();
			isEating();
			releaseChops();
			room.sortir();
		}
	}
	
	private void isThinking() {
		table.isThinking(id);
		System.out.println(getName() + " thinks");
		try {
			sleep((long) (Math.random() * TIME_THINK_MAX));
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		System.out.println(getName() + " finished thinking");
	}

	private void wantsFood() {
		System.out.println(getName() + " is hungry");
		table.isHungry(id);
	}
	
	private void getChops() {
		System.out.println(getName() + " wants left chopstick");
		left.take();
		table.takeChopstick(id, left.getID());
		System.out.println(getName() + " got left chopstick");
		try {
			sleep(TIME_NEXT_FORK);
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		System.out.println(getName() + " wants right chopstick");
		right.take();
		table.takeChopstick(id, right.getID());
		System.out.println(getName() + " got right chopstick");
	}
	
	private void isEating() {
		System.out.println(getName() + " eats");
		try {
			sleep((long) (Math.random() * TIME_EAT_MAX));
		} catch (InterruptedException e) {
			System.out.println(e);
		}
		System.out.println(getName() + " finished eating");
	}
	
	private void releaseChops() {
		table.releaseChopstick(id, left.getID());
		left.release();
		System.out.println(getName() + " released left chopstick");

		table.releaseChopstick(id, right.getID());
		right.release();
		System.out.println(getName() + " released right chopstick");
	}
}
